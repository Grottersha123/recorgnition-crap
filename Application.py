import sys

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QLabel, QGridLayout, QListWidget, \
    QFileDialog, QRadioButton
from PyQt5.QtGui import QPixmap, QFont, QTransform
from PyQt5.QtCore import Qt

from full_network import NetWork
from MyMpl import *
from crop import *

class ApplicationWindow(QMainWindow):
    """
    Чтобы понять какая кнопка из двух нажата можно посомтреть значение переменной self.radio_button_state
    Если оно равно False, то загружены необрезанные фотографии (preprocessing)
    Если оно равно True, то загружены уже обрезанные фотографии (simple_divide)
    """
    def __init__(self):
        QMainWindow.__init__(self)
        self.file_name = None
        self.num_classes = 6
        self.new_win = QCrop()
        self.originalWidth = 2272
        self.originalHeight = 1704
        self.photo_size = self.originalWidth / 6, self.originalHeight / 6

        self.network = NetWork()

        self.file_names = list()
        self.photo_predictions = dict()

        self.setAttribute(Qt.WA_DeleteOnClose)
        self.main_widget = QWidget(self)

        self.bar_graph = MyDynamicMplCanvas(self.main_widget, color="red", width=7, height=5, dpi=100)
        self.result_diagram = MyDynamicMplCanvas(self.main_widget, color="green", width=7, height=5, dpi=100)

        self.choose_files_button = QPushButton("Выбрать файлы")
        self.clear_files_button = QPushButton("Удалить файлы")
        self.result_button = QPushButton("Получить результат")
        self.choose_files_button.clicked.connect(self.choose_files)
        self.clear_files_button.clicked.connect(self.clear_widget)
        self.result_button.clicked.connect(self.get_result)

        font = QFont("Arial", 16)
        self.photo_probability_label = QLabel("Вероятность для одного фото")
        self.total_probability_label = QLabel("Вероятность для партии")
        self.photo_probability_label.setFont(font)
        self.photo_probability_label.setAlignment(Qt.AlignCenter)
        self.total_probability_label.setFont(font)
        self.total_probability_label.setAlignment(Qt.AlignCenter)

        self.picture_label = QLabel()
        self.picture_label.setMinimumSize(*self.photo_size)
        self.picture_label.setScaledContents(True)

        self.list_widget = QListWidget()
        self.list_widget.itemSelectionChanged.connect(self.item_changed)

        self.simple_divide_radio_button = QRadioButton("Обрезанные фото")
        self.preprocessing_radio_button = QRadioButton("Необрезанные фото")
        self.preprocessing_radio_button.setChecked(True)
        self.radio_button_state = False
        self.simple_divide_radio_button.toggled.connect(self.simple_divide_button_clicked)
        self.preprocessing_radio_button.toggled.connect(self.preprocessing_button_clicked)

        self.layout = QGridLayout(self.main_widget)
        "позиция в столбце, позиция в строке, ширина по столбцу, ширина по строке"
        self.layout.addWidget(self.picture_label, 0, 0, 3, 1)
        self.layout.addWidget(self.choose_files_button, 0, 1, 1, 2)
        self.layout.addWidget(self.result_button, 0, 3, 1, 2)
        self.layout.addWidget(self.clear_files_button, 0, 5, 1, 2)
        self.layout.addWidget(self.preprocessing_radio_button, 1, 2, 1, 2)
        self.layout.addWidget(self.simple_divide_radio_button, 1, 4, 1, 3)
        self.layout.addWidget(self.list_widget, 2, 1, 1, 6)
        self.layout.addWidget(self.photo_probability_label, 3, 0, 1, 1)
        self.layout.addWidget(self.total_probability_label, 3, 1, 1, 6)
        self.layout.addWidget(self.bar_graph, 4, 0, 2, 1)
        self.layout.addWidget(self.result_diagram, 4, 1, 1, 6)

        self.setMinimumSize(1000, 600)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.set_photo("images/logo.jpg")

        # --------------

        self.edit_results_button = QPushButton("Редактировать")
        self.save_results_button = QPushButton("Сохранить")
        self.layout.addWidget(self.edit_results_button, 5, 2, 1, 2)
        self.layout.addWidget(self.save_results_button, 5, 4, 1, 2)
        self.edit_results_button.clicked.connect(self.openWin)


    def fileQuit(self):
        self.close()

    def get_result(self):
        if not self.file_names:
            return

        predicted_photos = set(list(self.photo_predictions.keys()))
        unpredicted_photos = [x for x in self.file_names if x not in predicted_photos]
        # unpredicted_photos = list(set(self.file_names) - set(list(self.photo_predictions.keys())))
        if not unpredicted_photos:
            return

        total_probabilities, photo_probabilities = self.network.predict_consignment(unpredicted_photos,
                                                                                    self.radio_button_state)

        for file_name, file_probability in zip(unpredicted_photos, photo_probabilities):
            self.photo_predictions[str(file_name)] = file_probability

        self.result_diagram.update_figure(total_probabilities)

    def closeEvent(self, ce):
        self.fileQuit()

    def openWin(self):
        print(self.file_name)
        self.new_win.setPath(self.file_name)
        self.new_win.show()

    def clear_widget(self):
        self.list_widget.clear()
        self.photo_predictions.clear()
        self.file_names.clear()
        self.picture_label.clear()
        self.result_diagram.clear()
        self.bar_graph.clear()
        self.result_diagram.axes.set_ylim(bottom=0, top=105)
        self.bar_graph.axes.set_ylim(bottom=0, top=105)
        self.set_photo("images/logo.jpg")

    def choose_files(self):
        dialog = QFileDialog()
        options = QFileDialog.Options()
        items, _ = QFileDialog.getOpenFileNames(dialog, "Открыть файлы", "", "Jpg Files (*.jpg)", options=options)
        self.list_widget.addItems(items)
        self.file_names = [str(self.list_widget.item(i).text()) for i in range(self.list_widget.count())]
        if len(self.file_names) > 0:
            self.file_name=str(self.file_names[0])

            self.set_photo(str(self.file_names[0]))

    def set_photo(self, path):
        pixmap = QPixmap(path)
        size = pixmap.size()

        if size.height() > size.width():
            transform = QTransform().rotate(90)
            pixmap = pixmap.transformed(transform, Qt.SmoothTransformation)

        self.picture_label.setPixmap(pixmap.scaled(*self.photo_size, Qt.KeepAspectRatio))

    def item_changed(self):
        item_text = self.list_widget.currentItem().text()
        self.bar_graph.axes.set_ylim(bottom=0, top=105)
        if self.photo_predictions.get(item_text) is not None:
            self.bar_graph.update_figure(self.photo_predictions[item_text])

        self.set_photo(item_text)
        self.file_name = item_text
        # print(self.file_name)

    def simple_divide_button_clicked(self, enabled):
        if enabled:
            self.radio_button_state = True

    def preprocessing_button_clicked(self, enabled):
        if enabled:
            self.radio_button_state = False


if __name__ == '__main__':
    app = QApplication(sys.argv)
    aw = ApplicationWindow()
    aw.setWindowTitle("Распознавание металлолома")
    aw.show()
    app.exec_()
