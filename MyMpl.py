import matplotlib
from PyQt5.QtWidgets import QSizePolicy
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
matplotlib.use("Qt5Agg")


class MyMplCanvas(FigureCanvas):

    def __init__(self, parent=None, color=None, labels=None, width=5, height=4, dpi=100):
        if color is None:
            color = "red"
        if labels is None:
            labels = ["2АШ", "3А", "3АЖД", "5АЖД", "6А", "9А"]
        self.color = color
        self.labels = labels
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        self.axes.set_ylim(bottom=0, top=105)

        self.axes.hold(False)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.axes.set_xticks(range(1, len(self.labels)+2))
        self.axes.set_xticklabels(self.labels)
        self.axes.grid(True)


class MyDynamicMplCanvas(MyMplCanvas):
    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        self.axes.set_ylim(bottom=0, top=105)
        self.axes.grid(True)

    def clear(self):
        xs = range(len(self.labels))
        self.axes.bar(xs, [0, 0, 0, 0, 0, 0], width=0.7, color=self.color, alpha=0.8, zorder=2)
        self.axes.set_xticks(range(len(self.labels)))
        self.axes.set_xticklabels(self.labels)
        self.axes.set_ylim(bottom=0, top=105)
        self.axes.grid(True)
        self.draw()

    def update_figure(self, result_array):
        # result_array = [i*100 for i in result_array]
        xs = range(len(self.labels))
        self.axes.bar(xs, result_array, width=0.7, color=self.color, alpha=0.8, zorder=2)
        self.axes.set_xticks(range(len(self.labels)))
        self.axes.set_xticklabels(self.labels)

        rects = self.axes.patches
        for rect, label in zip(rects, result_array):
            label = round(label, 2)
            if label == 0:
                continue
            label = str(label)
            label += "%"
            height = rect.get_height()
            self.axes.text(rect.get_x() + rect.get_width() / 2, height + 5, label, ha='center', va='bottom')

        self.axes.set_ylim(bottom=0, top=105)
        self.axes.grid(True)
        self.draw()
