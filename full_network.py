import numpy as np

from keras.models import load_model, Model,model_from_json

from sklearn.externals import joblib
import pickle

from image_preprocessing import get_preprocessing_areas


"""
внешняя функция
predict_consignment(files_path_list, simple_div=False)
принимает на вход список путей к файлу
возвращает 2 массива

первый - массив для всей партии в процентах

второй - двумерный массив, строки которого - результат для одного фото в процентах


"""

class NetWork():
    def __init__(self):
        json_file = open(r"model_support\4.json", 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        self.model.load_weights(r"model_support\4.h5")

        self.categories_num = 6

    def predict_part(self, image_part):
        image_part = np.array([image_part])
        features = self.model.predict(image_part)
        return np.argmax(features)

    def predict_image(self, file_path, simple_div=False):

        areas = get_preprocessing_areas(file_path, simple_div=simple_div)

        areas_results = np.zeros(self.categories_num)
        if len(areas) is 0:
            print(file_path, "No prediction")
            return areas_results
        else:
            for area in areas:
                prediction = int(self.predict_part(area))
                areas_results[prediction] += 1

        # приведение к процентной форме
        coeff = (100. / areas_results.sum())
        areas_results = areas_results * coeff

        return areas_results

    #предсказание для всей партии
    def predict_consignment(self, files_path_list, simple_div=False):
        image_predictions = []
        for file_path in files_path_list:
            image_predictions.append(self.predict_image(file_path, simple_div))

        image_predictions = np.array(image_predictions)
        consignment_result = np.zeros(self.categories_num)

        for prediction in image_predictions:
            consignment_result += prediction

        coeff = (100. / consignment_result.sum())
        consignment_result = consignment_result * coeff

        return consignment_result, image_predictions

