import sys
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QLabel, QGridLayout, QListWidget, \
    QFileDialog, QProgressBar, QStyleFactory,QRubberBand
import os

class QCrop (QLabel):
    rectChanged = QtCore.pyqtSignal(QtCore.QRect)
    def __init__(self,parentQWidget = None):
        self.path = None
        super(QCrop, self).__init__(parentQWidget)
        self.currentQRubberBand = QRubberBand(QRubberBand.Rectangle, self)


        self.changeRubberBand = False
#Устанавливается картинка
    def initUI (self):
        self.img = QtGui.QPixmap(self.path)
        self.setPixmap(self.img.scaled(self.img.width()//4,self.img.height()//4))

    def setPath(self,path):
        print(path)
        self.path = path
        print(self.path)
        self.initUI()

    def mousePressEvent (self, eventQMouseEvent):
            self.originQPoint = eventQMouseEvent.pos()
            self.currentQRubberBand.setGeometry(QtCore.QRect(self.originQPoint, QtCore.QSize()))
            self.rectChanged.emit(self.currentQRubberBand.geometry())
            self.currentQRubberBand.show()
            self.changeRubberBand = True


    def mouseMoveEvent (self, eventQMouseEvent):
        if self.changeRubberBand:
            self.currentQRubberBand.setGeometry(QtCore.QRect(self.originQPoint, eventQMouseEvent.pos()).normalized())


    def mouseReleaseEvent (self, eventQMouseEvent):
            if self.currentQRubberBand.isVisible():
                currentQRect = self.currentQRubberBand.geometry()
                a,b,c,d = currentQRect.getRect()
                cropQPixmap = self.img.copy(QtCore.QRect(a*4,b*4,c*4,d*4))
                if not os.path.exists(r'screenshot'):
                    os.mkdir(r'screenshot')
                cropQPixmap.save('screenshot\sc_{}'.format(os.path.split(self.path)[-1]))

            self.changeRubberBand = False
            QWidget.mouseReleaseEvent(self,eventQMouseEvent)
#Запускает программу
#name принимает путь файла
# Программа работает так выделяешь область и закрываешь программу
# все изображения сохраняются в папке screenshot с таким же именем как у файла

def run(name):
    print(name)
    if name:
        print(name,'crop')
        myQApplication = QApplication(sys.argv)
        myQExampleLabel = QCrop(name)
        myQExampleLabel.show()
        # myQApplication.


if __name__ == '__main__':
    run(r'D:\Git_project\recorgnition-crap\PICS\1\0_1.jpg')

